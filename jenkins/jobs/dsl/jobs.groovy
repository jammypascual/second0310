def workspaceFolderName = "${WORKSPACE_NAME}"
def projectFolderName = "${PROJECT_NAME}" 
def samplefolderName = projectFolderName + "/SampleFolder"
def samplefolder = folder(samplefolderName) { displayName('Folder created by Jenkins') }

//Jobs
def Jobs1 = freeStyleJob(samplefolderName + "/FirstJob")
def Jobs2 = freeStyleJob(samplefolderName + "/SecondJob")

//Pipeline
def samplepipe = buildPipelineView(samplefolderName + "/Sample-Pipeline-Demo")

	samplepipe.with{
		title('Pipeline Demo')
		displayedBuilds(5)
		selectedJob(samplefolderName + "/FirstJob")
		showPipelineParameters()
		refreshFrequency(5)
}

//Job Configuration
Jobs1.with{
		scm{
			git{
		
				remote{
					url('https://jammypascual:Jfp062793@bitbucket.org/jammypascual/second0310.git')
						credentials("adop-jenkins-master")
					}
					branch("*/master")
				}
			}
	steps{
	shell('''#!/bin/sh
	ls -lart''')
	}

publishers{
	downstreamParameterized{
				trigger(samplefolderName + "/SecondJob"){
				condition("SUCCESS")
				parameters{
						predefinedProp("CUSTOM_WORKSPACE",'$WORKSPACE')
					   }
				}

			}		
		}
	}
Jobs2.with{
		parameters{
			stringParam("CUSTOM_WORKSPACE","","")
}
	steps{
	shell('''#!/bin/sh
	ls -lart $CUSTOM_WORKSPACE
	''')
	}
}